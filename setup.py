from setuptools import setup

setup(
    name='matrixinterp',
    version='0.2.0',
    packages=['matrixinterp'],
    url='https://gitlab.com/ernesto-ocampo/python-matrix-interpolation',
    license='',
    author='ernesto-ocampo',
    author_email='ocampoernesto@gmail.com',
    description='Library and tool for interpolating missing values in matrices.',
    install_requires=[
        'pytest',
        'lettuce==0.2.23',
        'numpy==1.12.0'
    ]
)
