Feature: Tool command line interface
    In order to invoke the interpolation tool
    As users of the interpolation tool
    We'll implement a file based-interface where the tools receives two
    compulsory command line arguments: infile and outfile.

    Scenario: No arguments given to tool
      Given I do not specify any arguments
      When I invoke the tool
      Then I get exit code 2

    Scenario: Help argument given to tool
      Given I specify the help argument -h
      When I invoke the tool
      Then I get a help message
        And I get exit code 0

    Scenario: One argument given
      Given I specify arguments some-arg
      When I invoke the tool
      Then I get exit code 2

    Scenario: Both arguments are invalid paths
      Given I specify arguments invalid1 invalid2
      When I invoke the tool
      Then I get exit code 3

    Scenario: Input argument is invalid path
      Given An empty file valid_path
        And I specify arguments invalid valid_path
      When I invoke the tool
      Then I get exit code 3

    Scenario: Input argument is a directory path
      Given An empty file valid_path
        And directory valid_dir exists
        And I specify arguments valid_dir valid_path
      When I invoke the tool
      Then I get exit code 3

    Scenario: Output argument is a directory path
      Given An empty file valid_path
        And directory valid_dir exists
        And I specify arguments valid_path valid_dir
      When I invoke the tool
      Then I get exit code 3

    Scenario: Output argument is invalid path - output should be generated
      Given An empty file valid_path
        And I specify arguments valid_path out_path
      When I invoke the tool
      Then I get exit code 0
        And File out_path exists
