import os

from lettuce import step, world

from matrixinterp.tests.test_utils import write_matrix, read_matrix, read_matrix_from_lines
from feature_test_utils import create_tmp_dir, clean_tmp_dir


@step("I use the iterative option")
def step_impl(step):
    world.tool_args.append('-iter')
