import os

from lettuce import step, world


@step('I do not specify any arguments')
def i_do_not_specify_args(step):
    world.tool_args = []


@step("I specify the help argument ([^ ]+)")
def step_impl(step, arg):
    world.tool_args = [arg]


@step("I get a help message")
def step_impl(step):
    assert 'usage' in world.tool_out


@step("An empty file ([^ ]+)")
def step_impl(step, file_path):
    abs_path = os.path.join(world.tmp_path, file_path)
    if os.path.exists(abs_path) and not os.path.isfile(abs_path):
        raise Exception("Can't create directory {} because a non-file"
                        " with that paths exists".format(abs_path))
    else:  # ensure empty
        with open(os.path.join(world.tmp_path, file_path), 'w'):
            pass


@step("directory ([^ ]+) exists")
def step_impl(step, dir_path):
    abs_path = os.path.join(world.tmp_path, dir_path)
    if os.path.exists(abs_path):
        if not os.path.isdir(abs_path):
            raise Exception("Can't create directory {} because a"
                            "non-dir with that paths exists".format(dir_path))
    else:
        os.makedirs(abs_path)


@step("File ([^ ]+) exists")
def step_impl(step, file_path):
    assert os.path.isfile(os.path.join(world.tmp_path, file_path))