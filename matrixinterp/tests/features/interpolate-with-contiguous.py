import os

from lettuce import step, world

from matrixinterp.tests.test_utils import write_matrix, read_matrix, read_matrix_from_lines
from feature_test_utils import create_tmp_dir, clean_tmp_dir


@step("The input matrix:")
def step_impl(step):
    path = os.path.join(world.tmp_path, 'in.csv')
    matrix = read_matrix_from_lines(step.multiline.split('\n'))
    write_matrix(matrix, path)
    world.tool_args[0] = path


@step("I get this output:")
def step_impl(step):
    actual = read_matrix(world.tool_args[1])
    expected = read_matrix_from_lines(step.multiline.split('\n'))
    assert actual == expected, \
        "\nGot      {},\nexpected {}".format(actual, expected)


@step("I use the contiguous option")
def step_impl(step):
    world.tool_args.append('-cont')
