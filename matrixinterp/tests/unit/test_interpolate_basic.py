import inspect
import pytest

import numpy

from matrixinterp.interpolators import (
    InterpolationError,
    NonContiguousInterpolator
)


def interpolate_matrix(matrix):
    return NonContiguousInterpolator().interpolate(matrix)


def test_interpolator_class_exists():
    assert inspect.isclass(NonContiguousInterpolator)


def test_contiguous_missing():
    data = [[1.5, 4.0, 2.0],
            [3.5, float('nan'), float('nan')],
            [6.0, 7.0, 8.0]]
    for i in range(4):
        matrix = numpy.rot90(numpy.matrix(data), i)
        with pytest.raises(InterpolationError):
            interpolate_matrix(matrix)
