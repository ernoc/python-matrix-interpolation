import pytest

import numpy

from matrixinterp.interpolators import IterativeInterpolator

@pytest.fixture
def interpolator(request):
    yield IterativeInterpolator()


def test_iterate(interpolator):
    data = [[1.0, float('nan')],
            [float('nan'), float('nan')]]
    expected = [[1.0, 1.0], [1.0, 1.0]]
    for i in range(4):
        matrix = numpy.rot90(numpy.matrix(data), i)
        interpolator.interpolate(matrix)
        assert (matrix == numpy.rot90(numpy.matrix(expected), i)).all()
