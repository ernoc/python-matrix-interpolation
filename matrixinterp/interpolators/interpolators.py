from abc import ABCMeta, abstractmethod

import math

import numpy


class InterpolationError(ValueError):
    pass


class MatrixInterpolator(object):
    __metaclass__ = ABCMeta

    @abstractmethod
    def interpolate(self, matrix):
        """Completes the given numpy matrix by interpolating missing values

        Interpolation is performed in-place: the supplied instance of the matrix
        is modified, filling the missing values.
        :param matrix: a matrix - must contain concrete float values,
        or missing values denoted by float('nan'). If it matrix contains
        contiguous one 'nan' value, an InterpolationError is raised.
        :type matrix: numpy.ndarray
        :returns: None - the supplied matrix is updated.
        """
        pass


class NonContiguousInterpolator(MatrixInterpolator):
    @staticmethod
    def _get_neighbours_avg(matrix, row, col):
        """Computes average value of non-diagonal adjacent values
        to matrix.item((row, col)). If any of those values is nan,
        raises an InterpolationError
        """
        sum = 0.0
        num = 0.0
        rows, cols = matrix.shape
        neighbours = [(row - 1, col), (row, col + 1), (row + 1, col), (row, col - 1)]
        for n_row, n_col in neighbours:
            if 0 <= n_row < rows and 0 <= n_col < cols:
                if math.isnan(matrix.item((n_row, n_col))):
                    raise InterpolationError('Contiguous missing values')
                sum += matrix.item(n_row, n_col)
                num += 1
        return sum / num

    def interpolate(self, matrix):
        """Completes the given numpy matrix by interpolating missing values

        Interpolation is performed in-place: the supplied instance of the matrix
        is modified, filling the missing values.
        This routine runs in linear time (O(n)) on the number of elements in the
        matrix a requires constant (O(1)) extra space.

        :param matrix: a matrix - must contain concrete float values,
        or missing values denoted by float('nan'). Values are interpolated as the
        average of the non-diagonal adjacent values. This matrix is to be
        updated by filling missing value. If it matrix contains contiguous
        missing values, or the matrix comprises only one 'nan' value,
        an InterpolationError is raised.
        :type matrix: numpy.ndarray
        :returns: None - the supplied matrix is updated.
        """
        if matrix.size == 0:
            return
        if matrix.size == 1 and math.isnan(matrix.item(0, )):
            raise InterpolationError('The only value in the matrix is nan')

        rows, cols = matrix.shape
        for row in range(rows):
            for col in range(cols):
                if math.isnan(matrix.item((row, col))):
                    matrix[row, col] = self._get_neighbours_avg(
                        matrix, row, col)


class AdvancedInterpolator(MatrixInterpolator):
    @staticmethod
    def _get_non_nan_col(matrix, row, col):
        """Gets the next non-nan column starting from row,col

        :param matrix: a matrix (2d shape assumed)
        :param row: integer, assumed in range
        :param col: integer, assumed in range
        """
        _, cols = matrix.shape
        while col < cols and math.isnan(matrix.item(row, col)):
            col += 1
        return col if col < cols else None

    @staticmethod
    def _get_non_nan_rows(matrix, row, col):
        """Gets the next and previous non-nan row starting from row,col

        :param matrix: a matrix (2d shape assumed)
        :param row: integer, assumed in range
        :param col: integer, assumed in range
        :return: a tuple (up, down) with the previous and next
        columns that have non-nan values, or None.
        """
        rows, _ = matrix.shape
        up = row
        down = row
        while up >= 0 and math.isnan(matrix.item(up, col)):
            up -= 1
        while down < rows and math.isnan(matrix.item(down, col)):
            down += 1
        return up if up >= 0 else None, down if down < rows else None

    @staticmethod
    def _horz_interp(matrix, row, col, left, right):
        """Performs horizontal interpolation of value at row,col
        between concrete given values at columns left and right.
        :return a tuple (points, interp) with the number of concrete
        points used (0, 1, or 2) and the interpolated value."""
        if left is None and right is None:
            return 0, None
        elif left is None:
            return 1, matrix.item(row, right)
        elif right is None:
            return 1, matrix.item(row, left)
        else:
            interval_diff = matrix.item(row, right) - matrix.item(row, left)
            interval_size = float(right - left)
            return 2, matrix.item(row, left) + \
                   (interval_diff / interval_size) * (col - left)

    @staticmethod
    def _vert_interp(matrix, row, col, up, down):
        """Performs vertical interpolation of value at row,col
        between concrete given values at rows up and down.
        :return a tuple (points, interp) with the number of concrete
        points used (0, 1, or 2) and the interpolated value.
        This method has a lot in common with _horz_interpolate
        but any attempt to reuse and extract common features
        will hinder readability."""
        if up is None and down is None:
            return 0, None
        elif up is None:
            return 1, matrix.item(down, col)
        elif down is None:
            return 1, matrix.item(up, col)
        else:
            interval_diff = matrix.item(down, col) - matrix.item(up, col)
            interval_size = float(down - up)
            return 2, matrix.item(up, col) + \
                   (interval_diff / interval_size) * (row - up)

    def _interp(self, matrix, row, col, left, right, up, down):
        hpoints, horz_inter = self._horz_interp(matrix, row, col, left, right)
        vpoints, vert_inter = self._vert_interp(matrix, row, col, up, down)
        if horz_inter is None and vert_inter is None:
            return float('nan')
        elif horz_inter is None:
            return vert_inter
        elif vert_inter is None:
            return horz_inter
        else:
            return (hpoints * horz_inter + vpoints * vert_inter) / \
                   (hpoints + vpoints)

    def _get_interpolations(self, matrix):
        """Gets the set of interpolated values to apply"""
        if matrix.size == 0:
            return None
        if all(math.isnan(val) for val in numpy.nditer(matrix)):
            raise InterpolationError('All values in the matrix are nan')

        changes = {}  # a map of (row, col) coordinate to new values

        rows, cols = matrix.shape
        for row in range(rows):
            left = None  # where the left bound is for horiz intplation
            right = None
            for col in range(cols):
                if not math.isnan(matrix.item(row, col)):
                    left = col
                    right = None
                else:
                    if right is None:
                        right = self._get_non_nan_col(matrix, row, col)

                    # this could be sped up using dynamic programming:
                    up, down = self._get_non_nan_rows(matrix, row, col)

                    changes[(row, col)] = self._interp(matrix, row, col, left,
                                                       right, up, down)
        return changes

    @staticmethod
    def _apply_changes(matrix, changes):
        if changes:
            for (row, col), value in changes.iteritems():
                if math.isnan(value):
                    raise InterpolationError(
                        'All values in row {} and column {} are nan'
                        .format(row, col))
                matrix[row, col] = value

    def interpolate(self, matrix):
        """Completes the given numpy matrix by interpolating missing values

        Interpolation is performed in-place: the supplied instance of the matrix
        is modified, filling the missing values.
        This routine runs in quadratic time (O(n^2)) worst case
        on the number of elements in the
        matrix a requires (O(n)) extra space.

        :param matrix: a matrix - must contain concrete float values,
        or missing values denoted by float('nan'). Values are interpolated
        linearly using the available horizontal and vertical concrete values
        in the same row / column.
        This matrix is to be updated by filling missing value. If it matrix
        comprises only one 'nan' values, or if a row and a column contain
        only 'nan' values, an InterpolationError is raised.
        :type matrix: numpy.ndarray
        :returns: None - the supplied matrix is updated.
        """
        changes = self._get_interpolations(matrix)

        # apply changes in the end so interpolated values do
        # not affect interpolations
        self._apply_changes(matrix, changes)


class IterativeInterpolator(AdvancedInterpolator):
    def interpolate(self, matrix):
        while True:
            changes = self._get_interpolations(matrix)
            if not changes:
                break
            self._apply_changes(matrix, changes)

    @staticmethod
    def _apply_changes(matrix, changes):
        if changes:
            for (row, col), value in changes.iteritems():
                matrix[row, col] = value
